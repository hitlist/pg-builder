'use strict'

const {test, threw} = require('tap')
const config = require('keef')
const Postgres = require('@allstar/postgres')
const PGQuery = require('../lib')


const SQL = 'SELECT * from pgquery'

let db = null

test('connect db', (t) => {
  console.log(config.get('db'))
  db = new Postgres({
    host: config.get('db:host')
  , port: config.get('db:port')
  , database: config.get('db:name')
  , user: config.get('db:user')
  , password: config.get('db:password')
  })
  t.end()
})

test('array contains', async (t) => {
  t.plan(51)
  const query = new PGQuery({
    text: SQL
  , fields: {
      numbers: 'array'
    }
  })
  .where('numbers', null, 'ne')
  .contains('numbers', 5)
  .toJSON()

  t.equal(query.name, undefined, 'name is undefined')
  const res = await db.query(query)
  t.ok(res.rows.length)
  for (var idx = 0; idx < res.rows.length; idx++) {
    const row = res.rows[idx]
    t.ok(row.numbers.includes(5))
  }
}).catch(threw)


test('gt', async (t) => {
  const query = new PGQuery({
    text: SQL
  })
  .where('numbers', null, 'ne')
  .gt('incr', 80)
  .toJSON()

  const res = await db.query(query)
  t.ok(res.rows.length)
  for (var idx = 0; idx < res.rows.length; idx++) {
    t.ok(res.rows[idx].incr > 80)
  }
}).catch(threw)

test('isnull', async (t) => {
  await t.test('is null true', async (tt) => {
    tt.plan(99)
    const query = new PGQuery({
      text: SQL
    })
    .where('numbers', null, 'ne')
    .isnull('nullable', true)
    .toJSON()

    const res = await db.query(query)
    for (var idx = 0; idx < res.rows.length; idx++) {
      const row = res.rows[idx]
      tt.strictEqual(row.nullable, null, 'nullable')
    }
  })

  await t.test('is null false', async (tt) => {
    tt.plan(1)
    const query = new PGQuery({
      text: SQL
    })
    .where('numbers', null, 'ne')
    .isnull('nullable', false)
    .toJSON()

    const res = await db.query(query)
    for (var idx = 0; idx < res.rows.length; idx++) {
      const row = res.rows[idx]
      tt.strictEqual(row.nullable, 100, 'nullable')
    }
  })
}).catch(threw)

test('number range', async (t) => {
  const query = new PGQuery({
    text: SQL
  })
  .where('numbers', null, 'ne')
  .range('incr', [90, 95])
  .toJSON()

  const res = await db.query(query)
  for (var idx = 0; idx < res.rows.length; idx++) {
    const row = res.rows[idx]
    t.ok(row.incr >= 90, 'incr >= 90')
    t.ok(row.incr <= 95, 'incr <= 95')
  }
}).catch(threw)

test('date range', async (t) => {
  const query = new PGQuery({
    text: SQL
  })
  .where('numbers', null, 'ne')
  .range('created_at', ['2016-01-01', '2017-01-01'])
  .toJSON()

  const min = new Date('2016-01-01')
  const max = new Date('2017-01-01')

  const res = await db.query(query)
  for (var idx = 0; idx < res.rows.length; idx++) {
    const row = res.rows[idx]
    t.ok(row.created_at >= min, 'incr >= 2016-01-01')
    t.ok(row.created_at <= max, 'incr <= 2017-01-01')
  }
}).catch(threw)

test('Pageing functions', async (t) => {
  const query = new PGQuery({
    text: SQL
  })
  .where('numbers', null, 'ne')
  .orderby('incr', 'asc')
  .limit(10)
  .offset(10)
  .toJSON()

  const res = await db.query(query)
  t.equal(res.rows.length, 10, '10 records')
  res.rows.reduce((a, b) => {
    t.ok(b.incr > a.incr, 'b > a')
    t.ok(b.incr > 10, 'b > 10')
    return b
  }, { incr: 9 })
}).catch(threw)

test('compile', async (t) => {
  await t.test('full example AND', async (tt) => {
    const opts = {
      filter: {
        numbers: {
          contains: 6
        }
      , nullable: {
          isnull: true
        }
      , created_at: {
          range: ['2016-01-01', '2017-01-01']
        }
      }
    , order: {
        created_at: 'desc'
      , first_name: 'asc'
      }
    , page: {
        limit: 10
      , offset: 0
      }
    }
    const query = new PGQuery({
      text: SQL
    , fields: {
        numbers: 'array'
      }
    })
    .compile(opts)

    const clauses = [
      '( numbers @> $1 )'
    , '( nullable IS NULL )'
    , '( created_at BETWEEN $2 AND $3 )'
    ].join('\nAND ')
    const expected = [
      `${SQL}`
      , `WHERE (${clauses})`
      , 'ORDER BY created_at desc, first_name asc'
      , 'LIMIT 10'
    ].join('\n')

    tt.equal(query.toString(), expected, 'rendered sql')
    tt.deepEqual(query.values, ['{6}', '2016-01-01', '2017-01-01'])
    const res = await db.query(query.toJSON())
    tt.equal(res.rows.length, 10, 'record count')
    const min = new Date('2016-01-01')
    const max = new Date('2017-01-01')

    for (var idx = 0; idx < res.rows.length; idx++) {
      const row = res.rows[idx]
      tt.strictEqual(row.nullable, null, 'nullable')
      tt.ok(row.created_at < max, 'max date')
      tt.ok(row.created_at > min, 'min date')
      tt.ok(row.numbers.includes(6), 'numbers array')
    }
  })

  await t.test('OR query', async (tt) => {
    const opts = {
      filter: {
        email: {
          icontains: '.net'
        }
      , incr: {
          range: '1,6'
        , gt: 90
        }
      }
    , order: {
        incr: 'asc'
      }
    }
    const query = new PGQuery({
      text: SQL
    , operator: 'OR'
    })
    .compile(opts)
    const clauses = [
      '( email ILIKE \'%\' || $1 || \'%\' )'
    , '( incr BETWEEN $2 AND $3 )'
    , '( incr > $4 )'
    ].join('\nOR ')
    const expected = [
      `${SQL}`
    , `WHERE (${clauses})`
    , 'ORDER BY incr asc'
    ].join('\n')

    tt.equal(query.toString(), expected, 'rendered sql')
    tt.deepEqual(query.values, ['.net', 1, 6, 90], 'values array')
    const res = await db.query(query.toJSON())
    for (var idx = 0; idx < res.rows.length; idx++) {
      const row = res.rows[idx]
      tt.ok(
        row.email.indexOf('.net') !== -1
        || row.incr > 90
        || (row.incr >= 1 || row.incr <= 6)
      )
    }
    res.rows.reduce((a, b) => {
      tt.ok(b.incr > a.incr, 'incr order')
      return b
    }, {incr: 0})
  })

  await t.test('operator or', async (tt) => {
    const opts = {
      filter: {
        email: {
          icontains: '.net'
        }
      , incr: {
          range: '1,6'
        , gt: 90
        }
      }
    , operator: 'or'
    , order: {
        incr: 'asc'
      }
    }
    const query = new PGQuery({
      text: SQL
    })
    .compile(opts)
    const clauses = [
      '( email ILIKE \'%\' || $1 || \'%\' )'
    , '( incr BETWEEN $2 AND $3 )'
    , '( incr > $4 )'
    ].join('\nOR ')
    const expected = [
      `${SQL}`
    , `WHERE (${clauses})`
    , 'ORDER BY incr asc'
    ].join('\n')
    tt.equal(query.toString(), expected, 'rendered sql')
    tt.deepEqual(query.values, ['.net', 1, 6, 90], 'values array')

    const res = await db.query(query.toJSON())
    for (var idx = 0; idx < res.rows.length; idx++) {
      const row = res.rows[idx]
      tt.ok(
        row.email.indexOf('.net') !== -1
        || row.incr > 90
        || (row.incr >= 1 || row.incr <= 6)
      )
    }
    res.rows.reduce((a, b) => {
      tt.ok(b.incr > a.incr, 'incr order')
      return b
    }, {incr: 0})
  })

  await t.test('operator and', async (tt) => {
    const opts = {
      filter: {
        email: {
          icontains: '.net'
        }
      , incr: {
          range: '1,6'
        , gt: 90
        }
      }
    , operator: 'and'
    , order: {
        incr: 'asc'
      }
    }
    const query = new PGQuery({
      text: SQL
    })
    .compile(opts)
    const clauses = [
      '( email ILIKE \'%\' || $1 || \'%\' )'
    , '( incr BETWEEN $2 AND $3 )'
    , '( incr > $4 )'
    ].join('\nAND ')
    const expected = [
      `${SQL}`
    , `WHERE (${clauses})`
    , 'ORDER BY incr asc'
    ].join('\n')

    tt.equal(query.toString(), expected, 'rendered sql')
    tt.deepEqual(query.values, ['.net', 1, 6, 90], 'values array')
    const res = await db.query(query.toJSON())
    for (var idx = 0; idx < res.rows.length; idx++) {
      const row = res.rows[idx]
      tt.ok(
        row.email.indexOf('.net') !== -1
        || row.incr > 90
        || (row.incr >= 1 || row.incr <= 6)
      )
    }
    res.rows.reduce((a, b) => {
      tt.ok(b.incr > a.incr, 'incr order')
      return b
    }, {incr: 0})
  })
  await t.test('unspported filter type', async (tt) => {
    await tt.throws(() => {
      new PGQuery({
        text: SQL
      }).compile({
        filter: {
          incr: {
            foobar: 1
          }
        }
      })
    }, /Unsupported filter type\: foobar/)
  })
}).catch(threw)

test('with count', async (t) => {
  const query = new PGQuery({
    text: 'SELECT * FROM pgquery'
  })
  .where('organization_id', 'f2f31927-12a8-4000-8e73-7a28aaf8e27d')
  .gt('incr', 50)
  .limit(5)
  .withCount('pgquery', 'AND')

  const res = await db.queryOne(query)
  t.type(res.data, Array, 'Data array')
  t.type(res.total, 'number', 'total property')
  t.equal(res.data.length, 5, 'result count')
  t.equal(res.total, 25, 'total count')
  for (var idx = 0; idx < res.data.length; idx++) {
    const row = res.data[idx]
    t.equal(
      row.organization_id
    , 'f2f31927-12a8-4000-8e73-7a28aaf8e27d'
    , 'organization_id'
    )
    t.ok(
      row.incr > 50
    , 'incr > 50'
    )
  }

}).catch(threw)

test('with count - no where clause', async (t) => {
  const query = new PGQuery({
    text: 'select * from pgquery'
  })
  .withCount('pgquery')
  const res = await db.queryOne(query)
  // The data is non-sensical here
  // just testing that the SQL is right doesn't blow up
  t.match(res, {
    total: 100 // should return all records
  , data: Array
  })
}).catch(threw)

test('with count - empty set', async (t) => {
  const query = new PGQuery({
    text: 'SELECT * FROM pgquery'
  })
  .where('organization_id', 'f2f31927-12a8-4000-8e73-7a28aaf8e27d', 'eq')
  .gt('incr', 50)
  .limit(5)
  .offset(100)
  .withCount('pgquery', 'AND')

  const res = await db.queryOne(query)
  t.type(res.data, Array, 'Data array')
  t.type(res.total, 'number', 'total property')
  t.equal(res.data.length, 0, 'result count')
  t.equal(res.total, 25, 'total count')

})

test('with count - using where, limit, offset, order asc', async (t) => {
  const query = new PGQuery({
    text: 'SELECT * FROM pgquery'
  })
  .where('incr', 50, 'gt')
  .exact('organization_id', 'f2f31927-12a8-4000-8e73-7a28aaf8e27d')
  .limit(8)
  .offset(0)
  .orderby('incr', 'asc')
  .withCount('pgquery', 'AND')

  const res = await db.queryOne(query)
  t.type(res.data, Array, 'Data array')
  t.type(res.total, 'number', 'total property')
  t.equal(res.data.length, 8, 'result count')
  t.equal(res.total, 25, 'total count')
  let last = -Infinity
  for (var idx = 0; idx < res.data.length; idx++) {
    const row = res.data[idx]
    t.equal(
      row.organization_id
    , 'f2f31927-12a8-4000-8e73-7a28aaf8e27d'
    , 'organization_id'
    )
    t.ok(
      row.incr > 50
    , 'incr > 50'
    )
    t.ok(last < row.incr, `${last} < ${row.incr}`)
    last = row.incr
  }
}).catch(threw)

test('with count - using where, is not null, limit order desc', async (t) => {
  const query = new PGQuery({
    text: 'SELECT * FROM pgquery'
  })
  .where('organization_id', null, 'ne')
  .gt('incr', 50)
  .limit(5)
  .orderby('incr', 'desc')
  .withCount('pgquery', 'AND')

  const res = await db.queryOne(query)
  t.type(res.data, Array, 'Data array')
  t.type(res.total, 'number', 'total property')
  t.equal(res.data.length, 5, 'result count')
  t.equal(res.total, 50, 'total count')
  let last = Infinity
  for (var idx = 0; idx < res.data.length; idx++) {
    const row = res.data[idx]
    t.ok(last > row.incr, `${last} > ${row.incr}`)
    t.ok(
      row.incr > 50
    , 'incr > 50'
    )
    last = row.incr
  }
}).catch(threw)

test('with count - JOIN w/ aliases', async (t) => {
  const WIDGET_ID = '88a66f7f-02ec-423e-b59a-4be68eeb0c36'
  const SELECT_TABLES = `
    app_widgets aw
  INNER JOIN widgets w on w.id = aw.widget_id
  INNER JOIN apps on apps.id = aw.app_id
  `
  const query = new PGQuery({
    text: `
      SELECT
        w.name
      , w.id
      , w.id as widget_id
      , apps.name as app_name
      FROM
      ${SELECT_TABLES}
    `
  , alias: {
      name: 'w.name'
    , app: 'aw.app_id'
    , widget_id: 'w.widget_id'
    , widget: 'w.id'
    }
  })
  .where('name', null, 'ne')
  .exact('widget', WIDGET_ID)
  .orderby('name', 'asc')
  .limit(50)
  .withCount(SELECT_TABLES)
  const res = await db.queryOne(query)
  t.equal(res.total, 10, 'total==10')
  t.equal(res.total, res.data.length, 'total=data.length')
  for (var idx = 0; idx < res.data.length; idx++) {
    const row = res.data[idx]
    t.equal(row.id, WIDGET_ID, `row.id==${WIDGET_ID}`)
    t.ok(row.app_name, 'app name')
  }
}).catch(threw)

test('disconnect db', async (t) => {
  await db.close()
}).catch(threw)
