'use strict'

const tap = require('tap')
const PGQuery = require('../lib')
const test = tap.test
const SQL = 'select * from table'

test('Paginations', (t) => {
  t.test('No modification', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    const out = query.toString()
    tt.equal(out, SQL, 'rendered sql')
    tt.end()
  })

  t.test('Limit - No Offset', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .limit(25)
    const out = query.toString()
    const expected = `${SQL}\nLIMIT 25`
    tt.equal(out, expected, 'rendered sql')
    tt.end()
  })

  t.test('Limit w/ offset', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .limit(25)
    .offset(50)
    const out = query.toString()
    const expected = `${SQL}\nLIMIT 25\nOFFSET 50`
    tt.equal(out, expected, 'rendered sql')
    tt.end()
  })

  t.test('group by singular', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .groupby('column_a')
    const out = query.toString()
    const expected = `${SQL}\nGROUP BY column_a`
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [], 'values array')
    tt.end()
  })

  t.test('group by multiple', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .groupby('column_a')
    .groupby('column_b')
    const out = query.toString()
    const expected = `${SQL}\nGROUP BY column_a, column_b`
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [], 'values array')
    tt.end()
  })

  t.test('group by multiple aliased', (tt) => {
    const query = new PGQuery({
      text: SQL
    , alias: {
        column_a: 'm.a'
      , column_b: 't.b'
      }
    })
    .groupby('column_a')
    .groupby('column_b')
    const out = query.toString()
    const expected = `${SQL}\nGROUP BY m.a, t.b`
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [], 'values array')
    tt.end()
  })

  t.test('order by singluar', (tt) => {
    tt.test('unspecified direction', (ttt) => {
      const query = new PGQuery({
        text: SQL
      })
      .orderby('column_a')
      const out = query.toString()
      const expected = `${SQL}\nORDER BY column_a desc`
      ttt.equal(out, expected, 'rendered sql')
      ttt.deepEqual(query.values, [], 'values array')
      ttt.end()
    })

    tt.test('asc', (ttt) => {
      const query = new PGQuery({
        text: SQL
      })
      .orderby('column_a', 'asc')
      const out = query.toString()
      const expected = `${SQL}\nORDER BY column_a asc`
      ttt.equal(out, expected, 'rendered sql')
      ttt.deepEqual(query.values, [], 'values array')
      ttt.end()
    })

    tt.test('desc', (ttt) => {
      const query = new PGQuery({
        text: SQL
      })
      .orderby('column_a', 'desc')
      const out = query.toString()
      const expected = `${SQL}\nORDER BY column_a desc`
      ttt.equal(out, expected, 'rendered sql')
      ttt.deepEqual(query.values, [], 'values array')
      ttt.end()
    })
    tt.end()
  })

  t.test('order by multi', (tt) => {
    tt.test('unspecified direction', (ttt) => {
      const query = new PGQuery({
        text: SQL
      })
      .orderby('column_a')
      .orderby('column_b')
      const out = query.toString()
      const expected = `${SQL}\nORDER BY column_a desc, column_b desc`
      ttt.equal(out, expected, 'rendered sql')
      ttt.deepEqual(query.values, [], 'values array')
      ttt.end()
    })

    tt.test('asc', (ttt) => {
      const query = new PGQuery({
        text: SQL
      })
      .where('id', 100)
      .orderby('column_a', 'asc')
      .orderby('column_b', 'asc')
      const out = query.toString()
      const expected = [
        `${SQL}`
      , 'WHERE ( id = $1 )'
      , 'ORDER BY column_a asc, column_b asc'
      ].join('\n')
      ttt.equal(out, expected, 'rendered sql')
      ttt.deepEqual(query.values, [100], 'values array')
      ttt.end()
    })

    tt.test('desc', (ttt) => {
      const query = new PGQuery({
        text: SQL
      })
      .where('id', 100)
      .orderby('column_a', 'desc')
      .orderby('column_b', 'desc')
      const out = query.toString()
      const expected = [
        `${SQL}`
      , 'WHERE ( id = $1 )'
      , 'ORDER BY column_a desc, column_b desc'
      ].join('\n')
      ttt.equal(out, expected, 'rendered sql')
      ttt.deepEqual(query.values, [100], 'values array')
      ttt.end()
    })

    tt.test('multiple directions', (ttt) => {
      const query = new PGQuery({
        text: SQL
      })
      .where('id', 100)
      .orderby('column_a')
      .orderby('column_b', 'desc')
      .orderby('column_c', 'asc')
      const out = query.toString()
      const expected = [
        `${SQL}`
      , 'WHERE ( id = $1 )'
      , 'ORDER BY column_a desc, column_b desc, column_c asc'
      ].join('\n')
      ttt.equal(out, expected, 'rendered sql')
      ttt.deepEqual(query.values, [100], 'values array')
      ttt.end()
    })
    tt.end()
  })
  t.end()
})

test('#gt', (t) => {
  t.test('AND operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    , operator: 'AND'
    })
    .where('id', 50)
    .gt('a', 44)
    .gt('c', 10)
    .gt('created', '2014-01-01')

    const out = query.toString()
    const clauses = [
      '( a > $2 )'
    , '( c > $3 )'
    , '( created > $4 )'
    ].join('\nAND ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })

  t.test('OR operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    , operator: 'OR'
    })
    .where('id', 50)
    .gt('a', 44)
    .gt('c', 10)
    .gt('created', '2014-01-01')

    const out = query.toString()
    const clauses = [
      '( a > $2 )'
    , '( c > $3 )'
    , '( created > $4 )'
    ].join('\nOR ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })

  t.end()
})

test('#gte', (t) => {
  t.test('AND operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    , operator: 'AND'
    })
    .where('id', 50)
    .gte('a', 44)
    .gte('c', 10)
    .gte('created', '2014-01-01')

    const out = query.toString()
    const clauses = [
      '( a >= $2 )'
    , '( c >= $3 )'
    , '( created >= $4 )'
    ].join('\nAND ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out.trim(), expected.trim(), 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })
  t.test('OR operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    , operator: 'OR'
    })
    .where('id', 50)
    .gte('a', 44)
    .gte('c', 10)
    .gte('created', '2014-01-01')

    const out = query.toString()
    const clauses = [
      '( a >= $2 )'
    , '( c >= $3 )'
    , '( created >= $4 )'
    ].join('\nOR ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })

  t.end()
})

test('#lt', (t) => {
  t.test('AND operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 50)
    .lt('a', 44)
    .lt('c', 10)
    .lt('created', '2014-01-01')

    const out = query.toString('AND')
    const clauses = [
      '( a < $2 )'
    , '( c < $3 )'
    , '( created < $4 )'
    ].join('\nAND ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })
  t.test('OR operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 50)
    .lt('a', 44)
    .lt('c', 10)
    .lt('created', '2014-01-01')

    const out = query.toString('OR')
    const clauses = [
      '( a < $2 )'
    , '( c < $3 )'
    , '( created < $4 )'
    ].join('\nOR ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })

  t.end()
})

test('#lte', (t) => {
  t.test('AND operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    , operator: 'AND'
    })
    .where('id', 50)
    .lte('a', 44)
    .lte('c', 10)
    .lte('created', '2014-01-01')

    const out = query.toString()
    const clauses = [
      '( a <= $2 )'
    , '( c <= $3 )'
    , '( created <= $4 )'
    ].join('\nAND ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })
  t.test('OR operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    , operator: 'OR'
    })
    .where('id', 50)
    .lte('a', 44)
    .lte('c', 10)
    .lte('created', '2014-01-01')

    const out = query.toString('OR')
    const clauses = [
      '( a <= $2 )'
    , '( c <= $3 )'
    , '( created <= $4 )'
    ].join('\nOR ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })
  t.end()
})

test('#exact', (t) => {
  t.test('AND operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    , operator: 'AND'
    })
    .where('id', 50)
    .exact('a', 44)
    .exact('c', 10)
    .exact('created', '2014-01-01')

    const out = query.toString()
    const clauses = [
      '( a = $2 )'
    , '( c = $3 )'
    , '( created = $4 )'
    ].join('\nAND ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })

  t.test('OR operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    , operator: 'OR'
    })
    .where('id', 50)
    .exact('a', 44)
    .exact('c', 10)
    .exact('created', '2014-01-01')

    const out = query.toString('OR')
    const clauses = [
      '( a = $2 )'
    , '( c = $3 )'
    , '( created = $4 )'
    ].join('\nOR ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
    tt.end()
  })
  t.end()
})

test('#iexact', (t) => {
  t.test('AND operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    , operator: 'AND'
    })
    .where('id', 50)
    .iexact('a', 'test')
    .iexact('c', 10)

    const out = query.toString()
    const clauses = [
      '( LOWER(a) = LOWER($2) )'
    , '( c = $3 )'
    ].join('\nAND ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 'test', 10])
    tt.end()
  })

  t.test('OR operator', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 50)
    .iexact('a', 'test')
    .iexact('c', 10)

    const out = query.toString('OR')
    const clauses = [
      '( LOWER(a) = LOWER($2) )'
    , '( c = $3 )'
    ].join('\nOR ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [50, 'test', 10])
    tt.end()
  })
  t.end()
})

test('#contains', (t) => {
  const query = new PGQuery({
    text: SQL
  , operator: 'OR'
  })
  .where('id', 'test')
  .where('id', 'x')
  .contains('email', 'foo%bar@mail.com')
  .contains('test', 'help')

  const out = query.toString()
  const clauses = [
    '( email LIKE \'%\' || $3 || \'%\' )'
  , '( test LIKE \'%\' || $4 || \'%\' )'
  ].join('\nOR ')
  const expected = [
    `${SQL}`
  , 'WHERE ( id = $1 )'
  , 'AND ( id = $2 )'
  , `AND (${clauses})`
  ].join('\n')
  t.equal(out, expected, 'rendered sql')
  t.deepEqual(query.values, ['test', 'x', 'foo%bar@mail.com', 'help'])
  t.end()
})

test('#icontains', (t) => {
  const query = new PGQuery({
    text: SQL
  })
  .where('id', 'test')
  .where('id', 'x')
  .icontains('email', 'foo%bar@mail.com')
  .icontains('test', 'help')

  const out = query.toString()
  const clauses = [
    '( email ILIKE \'%\' || $3 || \'%\' )'
  , '( test ILIKE \'%\' || $4 || \'%\' )'
  ].join('\nAND ')
  const expected = [
    `${SQL}`
  , 'WHERE ( id = $1 )'
  , 'AND ( id = $2 )'
  , `AND (${clauses})`
  ].join('\n')
  t.equal(out, expected, 'rendered sql')
  t.deepEqual(query.values, ['test', 'x', 'foo%bar@mail.com', 'help'])
  t.end()
})

test('#startswith', (t) => {
  const query = new PGQuery({
    text: SQL
  , operator: 'OR'
  })
  .where('id', 'test')
  .startswith('email', 'foo%bar@mail.com')
  .startswith('test', 'he')

  const out = query.toString()
  const clauses = [
    '( email LIKE $2 || \'%\' )'
  , '( test LIKE $3 || \'%\' )'
  ].join('\nOR ')
  const expected = [
    `${SQL}`
  , 'WHERE ( id = $1 )'
  , `AND (${clauses})`
  ].join('\n')
  t.equal(out, expected, 'rendered sql')
  t.deepEqual(query.values, ['test', 'foo%bar@mail.com', 'he'])
  t.end()
})

test('#istartswith', (t) => {
  const query = new PGQuery({
    text: SQL
  })
  .where('id', 'test')
  .istartswith('email', 'foo%bar@mail.com')
  .istartswith('test', 'he')

  const out = query.toString()
  const clauses = [
    '( email ILIKE $2 || \'%\' )'
  , '( test ILIKE $3 || \'%\' )'
  ].join('\nAND ')
  const expected = [
    `${SQL}`
  , 'WHERE ( id = $1 )'
  , `AND (${clauses})`
  ].join('\n')
  t.equal(out, expected, 'rendered sql')
  t.deepEqual(query.values, ['test', 'foo%bar@mail.com', 'he'])
  t.end()
})

test('#endswith', (t) => {
  const query = new PGQuery({
    text: SQL
  , operator: 'OR'
  })
  .where('id', 'test')
  .endswith('email', 'foo%bar@mail.com')
  .endswith('test', 'he')

  const out = query.toString()
  const clauses = [
    '( email LIKE \'%\' || $2 )'
  , '( test LIKE \'%\' || $3 )'
  ].join('\nOR ')
  const expected = [
    `${SQL}`
  , 'WHERE ( id = $1 )'
  , `AND (${clauses})`
  ].join('\n')
  t.equal(out, expected, 'rendered sql')
  t.deepEqual(query.values, ['test', 'foo%bar@mail.com', 'he'])
  t.end()
})

test('#iendswith', (t) => {
  const query = new PGQuery({
    text: SQL
  })
  .where('id', 'test')
  .iendswith('email', 'foo%bar@mail.com')
  .iendswith('test', 'he')

  const out = query.toString()
  const clauses = [
    '( email ILIKE \'%\' || $2 )'
  , '( test ILIKE \'%\' || $3 )'
  ].join('\nAND ')
  const expected = [
    `${SQL}`
  , 'WHERE ( id = $1 )'
  , `AND (${clauses})`
  ].join('\n')
  t.equal(out, expected, 'rendered sql')
  t.deepEqual(query.values, ['test', 'foo%bar@mail.com', 'he'])
  t.end()
})

test('#isnull', (t) => {
  t.test('isnull false', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .isnull('foobar', false)
    const out = query.toString()
    const clause = '( foobar IS NOT NULL )'
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clause})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1], 'values array')
    tt.end()
  })
  t.test('isnull true', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .isnull('foobar', 'true')
    const out = query.toString()
    const clause = '( foobar IS NULL )'
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clause})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1], 'values array')
    tt.end()
  })
  t.test('isnull \'true\'', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .isnull('foobar', true)
    const out = query.toString()
    const clause = '( foobar IS NULL )'
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clause})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1], 'values array')
    tt.end()
  })
  t.test('isnull 0', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .isnull('foobar', 0)
    const out = query.toString()
    const clause = '( foobar IS NOT NULL )'
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clause})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1], 'values array')
    tt.end()
  })
  t.test('isnull 1', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .isnull('foobar', 1)
    const out = query.toString()
    const clause = '( foobar IS NULL )'
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clause})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1], 'values array')
    tt.end()
  })
  t.test('isnull \'1\'', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .isnull('foobar', '1')
    const out = query.toString()
    const clause = '( foobar IS NULL )'
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clause})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1], 'values array')
    tt.end()
  })
  t.end()
})

test('#nin', (t) => {
  t.test('nin() csv', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .nin('foo', '1,2,3')

    const out = query.toString()
    const expected = [
      SQL
    , 'WHERE ( id = $1 )'
    , 'AND (( '
      + 'foo NOT IN ($2, $3, $4)'
    + ' ))'
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, '1', '2', '3'], 'values array')
    tt.end()
  })

  t.test('nin() array', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .nin('foo', [1, 2, 3])

    const out = query.toString()
    const expected = [
      SQL
    , 'WHERE ( id = $1 )'
    , 'AND (( '
      + 'foo NOT IN ($2, $3, $4)'
    + ' ))'
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, 1, 2, 3], 'values array')
    tt.end()
  })

  t.test('nin() csv - Array Column', (tt) => {
    const query = new PGQuery({
      text: SQL
    , fields: {
        test: 'array'
      }
    })
    .where('id', 1)
    .nin('foo', [1, 2, 3])
    .nin('test', '5,4,3')

    const out = query.toString()
    const clauses = [
      '( foo NOT IN ($2, $3, $4) )'
    , '( NOT ( test && $5 ) )'
    ].join('\nAND ')

    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')

    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, 1, 2, 3, '{5,4,3}'], 'values array')
    tt.end()
  })

  t.test('nin() array - Array Column', (tt) => {
    const query = new PGQuery({
      text: SQL
    , fields: {
        test: 'array'
      }
    })
    .where('id', 1)
    .nin('foo', [1, 2, 3])
    .nin('test', [5, 4, 3])

    const out = query.toString()
    const clauses = [
      '( foo NOT IN ($2, $3, $4) )'
    , '( NOT ( test && $5 ) )'
    ].join('\nAND ')

    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, 1, 2, 3, '{5,4,3}'], 'values array')
    tt.end()
  })
  t.end()
})

test('#in', (t) => {
  t.test('in() csv', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .in('foo', '1,2,3')

    const out = query.toString()
    const expected = [
      SQL
    , 'WHERE ( id = $1 )'
    , 'AND (( '
      + 'foo IN ($2, $3, $4)'
    + ' ))'
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, '1', '2', '3'], 'values array')
    tt.end()
  })

  t.test('in() array', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .in('foo', [1, 2, 3])

    const out = query.toString()
    const expected = [
      SQL
    , 'WHERE ( id = $1 )'
    , 'AND (( '
      + 'foo IN ($2, $3, $4)'
    + ' ))'
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, 1, 2, 3], 'values array')
    tt.end()
  })

  t.test('in() csv - Array Column', (tt) => {
    const query = new PGQuery({
      text: SQL
    , fields: {
        test: 'array'
      }
    })
    .where('id', 1)
    .in('foo', [1, 2, 3])
    .in('test', '5,4,3')

    const out = query.toString()
    const clauses = [
      '( foo IN ($2, $3, $4) )'
    , '( test && $5 )'
    ].join('\nAND ')

    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, 1, 2, 3, '{5,4,3}'], 'values array')
    tt.end()
  })

  t.test('in() array - Array Column', (tt) => {
    const query = new PGQuery({
      text: SQL
    , fields: {
        test: 'array'
      }
    })
    .where('id', 1)
    .in('foo', [1, 2, 3])
    .in('test', [5, 4, 3])

    const out = query.toString()
    const clauses = [
      '( foo IN ($2, $3, $4) )'
    , '( test && $5 )'
    ].join('\nAND ')

    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, 1, 2, 3, '{5,4,3}'], 'values array')
    tt.end()
  })
  t.end()
})

test('#range', (t) => {
  t.test('range csv', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .range('max', '1,2')
    .range('age', '10,40')

    const out = query.toString()
    const clauses = [
      '( max BETWEEN $2 AND $3 )'
    , '( age BETWEEN $4 AND $5 )'
    ].join('\nAND ')

    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, '1', '2', '10', '40'], 'values array')
    tt.end()
  })

  t.test('range array', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .range('max', [1, 2])
    .range('age', [10, 40])

    const out = query.toString()
    const clauses = [
      '( max BETWEEN $2 AND $3 )'
    , '( age BETWEEN $4 AND $5 )'
    ].join('\nAND ')

    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')

    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, 1, 2, 10, 40], 'values array')
    tt.end()
  })

  t.test('single value range', (tt) => {
    const query = new PGQuery({
      text: SQL
    })
    .where('id', 1)
    .range('max', '1')

    const out = query.toString()
    const clauses = [
      '( max >= $2 )'
    ].join('\nAND ')
    const expected = [
      `${SQL}`
    , 'WHERE ( id = $1 )'
    , `AND (${clauses})`
    ].join('\n')
    tt.equal(out, expected, 'rendered sql')
    tt.deepEqual(query.values, [1, '1'], 'values array')
    tt.end()
  })
  t.end()
})

test('#eq', (t) => {
  const query = new PGQuery({
    text: SQL
  , operator: 'AND'
  })
  .where('id', 50)
  .eq('a', 44)
  .eq('c', 10)
  .eq('created', '2014-01-01')

  const out = query.toString()
  const clauses = [
    '( a = $2 )'
  , '( c = $3 )'
  , '( created = $4 )'
  ].join('\nAND ')
  const expected = `${SQL}\nWHERE ( id = $1 )\nAND (${clauses})`
  t.equal(out, expected, 'rendered sql')
  t.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
  t.end()
})

test('#ne', (t) => {
  const query = new PGQuery({
    text: SQL
  , operator: 'AND'
  })
  .where('id', 50)
  .ne('a', 44)
  .ne('c', 10)
  .ne('created', '2014-01-01')

  const out = query.toString()
  const clauses = [
    '( a <> $2 )'
  , '( c <> $3 )'
  , '( created <> $4 )'
  ].join('\nAND ')
  const expected = `${SQL}\nWHERE ( id = $1 )\nAND (${clauses})`
  t.equal(out, expected, 'rendered sql')
  t.deepEqual(query.values, [50, 44, 10, '2014-01-01'])
  t.end()
})

test('column alias', (t) => {
  const query = new PGQuery({
    text: SQL
  , alias: {
      foo: 'f.oo'
    , bar: 'b.ar'
    }
  })
  .where('id', 100)
  .eq('foo', 10)
  .ne('bar', 20)

  const out = query.toString()
  const clauses = [
    '( f.oo = $2 )'
  , '( b.ar <> $3 )'
  ].join('\nAND ')
  const expected = `${SQL}\nWHERE ( id = $1 )\nAND (${clauses})`
  t.equal(out, expected, 'rendered sql')
  t.deepEqual(query.values, [100, 10, 20], 'values array')
  t.end()
})


test('#toString invalid query operator', (t) => {
  t.throws(() => {
    new PGQuery({
      text: 'select * from table'
    }).toString('FOO')
  }, /Invalid query operator FOO/)
  t.end()
})

test('constructor invalid options', (t) => {
  t.throws(() => {
    new PGQuery()
  }, /options is required and must be an object/)

  t.throws(() => {
    new PGQuery({
      text: ''
    })
  }, /options.text is required and must be a string/)

  t.throws(() => {
    new PGQuery({
      text: null
    })
  }, /options.text is required and must be a string/)
  t.end()
})

test('#where null', (t) => {
  const sql = 'select * from pgquery'
  t.test('null default operator', (tt) => {
    const query = new PGQuery({
      text: sql
    })
    .where('id', null)
    .gt('foo', 1)
    .lt('bar', 2)

    const out = query.toString('OR')
    const clauses = [
      '( foo > $1 )'
    , '( bar < $2 )'
    ].join('\nOR ')
    const expected = `${sql}\nWHERE ( id IS NULL )\nAND (${clauses})`
    tt.equal(out, expected)
    tt.end()
  })
  t.test('null exact', (tt) => {
    const query = new PGQuery({
      text: sql
    })
    .where('id', null, 'exact')
    .gt('foo', 1)
    .lt('bar', 2)

    const out = query.toString('OR')
    const clauses = [
      '( foo > $1 )'
    , '( bar < $2 )'
    ].join('\nOR ')
    const expected = `${sql}\nWHERE ( id IS NULL )\nAND (${clauses})`
    tt.equal(out, expected)
    tt.end()
  })
  t.end()
})

test('#where invalid comparison operator', (t) => {
  t.throws(() => {
    new PGQuery({
      text: 'select * from table'
    })
      .where('id', 'testing', '!=')
  }, /Invalid comparison operator/)
  t.end()
})

test('#where invalid comparison operator, null', (t) => {
  t.throws(() => {
    new PGQuery({
      text: 'select * from table'
    })
      .where('id', null, 'gte')
  }, /Operator for NULL must be 'ne' or 'eq'/)
  t.end()
})
