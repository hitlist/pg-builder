'use strict'

const tap = require('tap')
const test = tap.test

test('main exports', (t) => {
  const Query = require('../')

  t.type(Query, 'function', 'main export class')
  t.equal(Query.name, 'Query', 'query class')
  t.type(Query.format, 'function', 'format function')
  t.type(Query.colorize, 'function', 'colorize function')
  t.type(Query.pprint, 'function', 'pprint function')
  t.end()
})
