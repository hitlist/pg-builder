-- #######################################
-- THIS IS ONLY FOR TESTING / DEVELOPMENT
-- #######################################
--

CREATE TABLE public.pgquery (
  id UUID DEFAULT uuid_generate_v4() NOT NULL
, organization_id UUID DEFAULT uuid_generate_v4() NOT NULL
, first_name varchar(255) NOT NULL
, last_name varchar(255) NOT NULL
, numbers int[] NOT NULL
, created_at TIMESTAMP WITH TIME ZONE NOT NULL
, nullable int NULL DEFAULT NULL
, email varchar(255) NOT NULL
, incr SERIAL UNIQUE NOT NULL
, CONSTRAINT customer_pk PRIMARY KEY (id)
);
