CREATE TABLE public.widgets (
  id UUID DEFAULT uuid_generate_v4() NOT NULL
, name TEXT NOT NULL
, CONSTRAINT widget_pk PRIMARY KEY (id)
);

CREATE TABLE public.apps (
  id UUID DEFAULT uuid_generate_v4() NOT NULL
, name TEXT NOT NULL
, CONSTRAINT app_pk PRIMARY KEY(id)
);

CREATE TABLE public.app_widgets (
  id UUID DEFAULT uuid_generate_v4() NOT NULL
, widget_id UUID references widgets (id) NOT NULL
, app_id UUID REFERENCES apps (id) NOT NULL
);
