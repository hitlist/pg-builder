'use strict'

const fs = require('fs')
const path = require('path')
const Postgres = require('@allstar/postgres')
const config = require('keef')
const noop = () => {}

module.exports = seed

async function seed() {
  const sql = fs.readFileSync(
    path.join(__dirname, 'data.sql')
  ).toString('utf8')

  const db = new Postgres({
    host: config.get('db:host')
  , port: config.get('db:port')
  , database: config.get('db:name')
  , user: config.get('db:user')
  , password: config.get('db:password')
  })

  await db.query({
    text: sql
  , values: []
  })
  await loadApps(db)
  await loadWidgets(db)
  await db.close()
  return null
}

async function loadWidgets(db) {
  console.log('seeding widgets')
  const widgets = fs.readFileSync(
    path.join(__dirname, 'widgets.sql')
  ).toString('utf8')
  const app_widgets = fs.readFileSync(
    path.join(__dirname, 'app_widgets.sql')
  ).toString('utf8')
  await db.query({
    text: widgets
  , values: []
  })
  await db.query({
    text: app_widgets
  , values: []
  })
}

async function loadApps(db, cb) {
  console.log('seeding apps')
  const apps = fs.readFileSync(
    path.join(__dirname, 'apps.sql')
  ).toString('utf8')

  await db.query({
    text: apps
  , values: []
  })
}

if (require.main === module) {
  seed()
    .then(() => {
      console.log('seed complete')
    })
    .catch(console.error)
}
