FROM node:8

RUN mkdir -p /opt/app
WORKDIR /opt/app

ARG NPM_TOKEN
ARG DB_USER
ARG DB_PASSWORD
ARG DB_PORT
ARG DB_HOST
ARG DB_NAME

COPY . /opt/app

RUN npm install
