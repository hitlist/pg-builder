'use strict'

exports.colorize = require('./colorize')
exports.format = require('./format')
