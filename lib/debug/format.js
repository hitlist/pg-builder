'use strict'

const step = '  '

module.exports = function formatSql(text) {
  let deep = 0
  let parens = 0
  let str = ''
  const query = []
  const quoted = text
    .replace(/\s{1,}/g, ' ')
    .replace(/\'/ig, '~::~\'')
    .split('~::~')

  for (var x = 0; x < quoted.length; x++) {
    query.push(...split_sql(quoted[x], step))
  }

  for (var ix = 0; ix < query.length; ix++) {
    parens = isSubquery(query[ix], parens)

    if (/\s{0,}\s{0,}SELECT\s{0,}/.test(query[ix])) {
      query[ix] = query[ix].replace(/\,/g, ',\n      ')
    }

    if (/\s{0,}\s{0,}SET\s{0,}/.test(query[ix])) {
      query[ix] = query[ix].replace(/\,/g, ',\n      ')
    }

    if (/\s{0,}\(\s{0,}SELECT\s{0,}/.test(query[ix])) {
      str += '\n' + step.repeat(++deep) + query[ix]
    } else if (/\'/.test(query[ix])) {
      if (parens < 1 && deep) deep--
      str += query[ix]
    }
    else {
      str += '\n' + step.repeat(deep) + query[ix]
      if (parens < 1 && deep) deep--
    }
  }
  str = str.replace(/^\n{1,}/, '').replace(/\n{1,}/g, '\n')
  return str
}

function isSubquery(str, depth) {
  return depth - (
    str.replace(/\(/g, '').length - str.replace(/\)/g, '').length
  )
}

function split_sql(str) {
  return (str.replace(/\s{1,}/g, ' ')
    .replace(/ AND /ig, '~::~    AND ')
    .replace(/ BETWEEN /ig, '~::~  BETWEEN ')
    .replace(/ CASE /ig, '~::~  CASE ')
    .replace(/ ELSE /ig, '~::~  ELSE ')
    .replace(/ END /ig, '~::~  END ')
    .replace(/ FROM /ig, '~::~FROM ')
    .replace(/ GROUP\s{1,}BY/ig, '~::~GROUP BY ')
    .replace(/ HAVING /ig, '~::~HAVING ')
    .replace(/ INTEGER /ig, ' INTEGER ')
    .replace(/ IN /ig, ' IN ')
    .replace(/ JOIN /ig, '~::~JOIN ')
    .replace(/ CROSS~::~{1,}JOIN /ig, '~::~CROSS JOIN ')
    .replace(/ INNER~::~{1,}JOIN /ig, '~::~INNER JOIN ')
    .replace(/ LEFT~::~{1,}JOIN /ig, '~::~LEFT JOIN ')
    .replace(/ RIGHT~::~{1,}JOIN /ig, '~::~RIGHT JOIN ')
    .replace(/ ON /ig, '~::~  ON ')
    .replace(/ OR /ig, '~::~    OR ')
    .replace(/ ORDER\s{1,}BY/ig, '~::~ORDER BY ')
    .replace(/ OVER /ig, '~::~  OVER ')
    .replace(/\(\s{0,}SELECT /ig, '~::~(SELECT ')
    .replace(/\)\s{0,}SELECT /ig, ')~::~SELECT ')
    .replace(/ THEN /ig, ' THEN~::~  ')
    .replace(/ UNION /ig, '~::~UNION~::~')
    .replace(/ USING /ig, '~::~USING ')
    .replace(/ WHEN /ig, '~::~  WHEN ')
    .replace(/ WHERE /ig, '~::~WHERE ')
    .replace(/ WITH /ig, '~::~WITH ')
    .replace(/ ALL /ig, ' ALL ')
    .replace(/ AS /ig, ' AS ')
    .replace(/ ASC /ig, ' ASC ')
    .replace(/ DESC /ig, ' DESC ')
    .replace(/ DISTINCT /ig, ' DISTINCT ')
    .replace(/ EXISTS /ig, ' EXISTS ')
    .replace(/ NOT /ig, ' NOT ')
    .replace(/ NULL /ig, ' NULL ')
    .replace(/ LIKE /ig, ' LIKE ')
    .replace(/\s{0,}SELECT /ig, 'SELECT ')
    .replace(/\s{0,}UPDATE /ig, 'UPDATE ')
    .replace(/ SET /ig, ' SET ')
    .replace(/~::~{1,}/g, '~::~')
    .split('~::~'))
};
