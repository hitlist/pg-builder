'use strict'

const IS_TTY = require('tty').isatty(1)
const clearStyle = '\x1b[0m'
const red = '\x1b[31m'
const green = '\x1b[32m'
const yellow = '\x1b[1;33m'
const magenta = '\x1b[1;35m'
const cyan = '\x1b[36m'
const BASE_WORDS = [
  'ABSOLUTE', 'ADD', 'AFTER', 'AGGREGATE', 'ALL', 'ALSO', 'ALTER'
, 'ALWAYS', 'ANALYSE', 'ANALYZE', 'AND', 'ANY', 'ARE', 'ARRAY'
, 'AS', 'ASC', 'AT', 'ATOMIC', 'AVG', 'BASE64'
, 'BEFORE', 'BEGIN', 'BETWEEN', 'BIGINT', 'BINARY', 'BIT', 'BIT_LENGTH'
, 'BLOB', 'BOOLEAN', 'BOTH', 'BREADTH', 'BY', 'CASCADE', 'CASE', 'CAST', 'CEIL'
, 'CEILING', 'CHAIN', 'CHAR', 'CHARACTER', 'CHECKPOINT'
, 'COLLATE', 'COLLATION', 'COLUMN', 'COLUMNS', 'COMMENT', 'COMMIT'
, 'CONFLICT', 'CONNECT', 'CONNECTION', 'CONSTRAINT', 'CONTAINS', 'CONTINUE'
, 'COPY', 'COUNT', 'CREATE', 'CROSS', 'CSV', 'CURRENT', 'CURSOR', 'DATE', 'DAY'
, 'DEC', 'DECIMAL', 'DECLARE', 'DEFAULT', 'DEFERRABLE', 'DEFERRED', 'DELETE'
, 'DELIMITER', 'DESC', 'DO', 'DOUBLE', 'DROP', 'EACH', 'ELSE', 'END'
, 'END_PARTITION', 'ENUM', 'EQUALS', 'EVENT', 'EVERY', 'EXCEPT', 'EXCEPTION'
, 'EXCLUDE', 'EXCLUDING', 'EXEC', 'EXPLAIN', 'EXTENSION', 'EXTRACT', 'FALSE'
, 'FILTER', 'FINAL', 'FIRST', 'FLOAT', 'FLOOR', 'FOR', 'FROM', 'FULL'
, 'FUNCTION', 'GET', 'GRANT', 'GROUP', 'HAVING', 'HOUR', 'IF', 'IGNORE'
, 'ILIKE', 'IN', 'INCREMENT', 'INDENT', 'INDEX', 'INITIALLY', 'INNER'
, 'INSENSITIVE', 'INSERT', 'INT', 'INTEGER', 'INTERSECT', 'INTERVAL', 'INTO'
, 'IS', 'ISNULL', 'JOIN', 'KEY', 'LANGUAGE', 'LARGE', 'LAST', 'LATERAL', 'LEFT'
, 'LENGTH', 'LIKE', 'LIKE_REGEX', 'LIMIT', 'LOCATION', 'LOCK', 'LOWER'
, 'MATERIALIZED', 'MERGE', 'MINUTE', 'MINVALUE', 'MONTH', 'NAME'
, 'NATURAL', 'NO', 'NONE', 'NOT', 'NOTHING', 'NOTIFY', 'NOTNULL', 'NOWAIT'
, 'NULL', 'NULLABLE', 'NUMBER', 'NUMERIC', 'OBJECT', 'OF'
, 'OFFSET', 'ON', 'ONLY', 'OR', 'ORDER', 'OUTER', 'OVER', 'OVERLAPS', 'PAD'
, 'PARTIAL', 'PARTITION', 'PRIMARY', 'RANGE', 'RANK', 'RECURSIVE', 'REFERENCES'
, 'REFERENCING', 'REINDEX', 'RELATIVE', 'RENAME', 'REPLACE', 'RETURN'
, 'RETURNING', 'RETURNS', 'ROLE', 'ROLLBACK', 'ROW', 'SAVEPOINT', 'SCHEMA'
, 'SCHEMA_NAME', 'SEARCH', 'SECOND', 'SELECT', 'SET', 'SHOW', 'SIZE', 'SKIP'
, 'SMALLINT', 'SOME', 'SQRT', 'STABLE', 'STANDALONE', 'START', 'STATE'
, 'STATEMENT', 'STATIC', 'STDIN', 'STDOUT', 'STORAGE', 'STRICT', 'SUBSTRING'
, 'SUM', 'TABLE', 'TEMPORARY', 'TEXT', 'THEN', 'TIES', 'TIME', 'TIMESTAMP'
, 'TO', 'TRANSACTION', 'TRANSFORM', 'TRIGGER', 'TRIM', 'TRIM_ARRAY', 'TRUE'
, 'TRUNCATE', 'TYPE', 'UNION', 'UNIQUE', 'UNNAMED', 'UNNEST', 'UNTIL'
, 'UPDATE', 'UPPER', 'USER', 'USER_DEFINED_TYPE_SCHEMA', 'USING', 'VACUUM'
, 'VALUE', 'VALUES', 'VARCHAR', 'VARYING', 'VIEW', 'VIEWS', 'VOLATILE', 'WHEN'
, 'WHENEVER', 'WHERE', 'WINDOW', 'WITH', 'WITHIN', 'WITHOUT', 'YEAR', 'YES'
, 'ZONE'
]

const KEY_WORDS = []
//adding lowercase keyword support
while (BASE_WORDS.length) {
  const word = BASE_WORDS.shift()
  KEY_WORDS.push(word, word.toLowerCase())
}

const PSQL_PARAM_EXP = /(\$\d+)/g
const PSQL_NUMBER_EXP = /(\s)(\d+)(?:\t|\ )+?/g
const PSQL_SPECIAL_EXP = /(=|%|\/|\*|-|,|;|:|\+|<|>|{|})/g
const PSQL_QUOTE_EXP = /(['].*?['])/g
const PSQL_FUNCTION_EXP = /(\w*?)\(/g
const PSQL_OPERATOR_EXP = /([\(\)])/g

module.exports = function highliteSQl(text) {
  if (!IS_TTY) return text
  let new_text = text
  new_text = text
    .replace(PSQL_PARAM_EXP, cyan + '$1' + clearStyle)
    .replace(PSQL_NUMBER_EXP, green + ' $2 ' + clearStyle)
    .replace(PSQL_SPECIAL_EXP, yellow + '$1' + clearStyle)
    .replace(PSQL_QUOTE_EXP, green + '$1' + clearStyle)
    .replace(PSQL_FUNCTION_EXP, red + '$1' + clearStyle + '(')
    .replace(PSQL_OPERATOR_EXP, yellow + '$1' + clearStyle)

  for (var i = 0; i < KEY_WORDS.length; i++) {
    const WORD_EXP = new RegExp('\\b' + KEY_WORDS[i] + '\\b', 'g')
    new_text = new_text.replace(
      WORD_EXP
    , magenta + KEY_WORDS[i].toUpperCase() + clearStyle
    )
  }
  return new_text
}
